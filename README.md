# UXPinAutomatedDemoExample.

## Description:
This small project contains bunch of tests against UXPin design platform.

Tests included:

 * log in the user with valid credentials (positive scenario)

 * log in the user with invalid credentials (negative scenario)

 * basic CRUD on the project (create project, update project title, delete project)

Test are grouped in TestSuite. Running them as suite or separately is possible.

### Test were created and run on:

* System: Linux Ubuntu 16.04 LTS
* Browsers: Firefox Quantum 61.0.1, Chrome 68.0.3440.84 (both 64-bit)
* Selenium WebDriver: 3.13.0
* geckodriver (for Firefox): 0.21.0
* chromedriver (for Chrome): 2.41
* Java SDK 9 or 10

### Prerequisites:

1. To run this tests you need to download/install:

    1.1 Java SDK 9 or 10

    1.2 Apache Maven

    1.3 IDE of your choice (Eclipse, IntelliJ IDEA)

    1.4 geckodriver/chromedriver added to the PATH

2. Clone UXPin tests Bitbucket repository
3. Open it in IDE of your choice
5. Install dependencies from __pom.xml__
4. Go to __utilities/Credential__ class and set up your valid UXPin email and password
5. To run Test Suite go to __testsuite/TestSuite__ class and run it from IDE.
6. To run test as Maven project, open Terminal where pom.xml is located and run command __mvn clean test -Dtest=TestSuite.class__