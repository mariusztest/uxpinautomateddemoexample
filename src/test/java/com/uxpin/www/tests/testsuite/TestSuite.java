package com.uxpin.www.tests.testsuite;

import com.uxpin.www.tests.loginlogout.LogInLogOutTest;
import com.uxpin.www.tests.projectcrud.ProjectCRUDTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({LogInLogOutTest.class, ProjectCRUDTest.class})
public class TestSuite {


}
