package com.uxpin.www.tests.projectcrud;

import com.uxpin.www.pages.BasePage;
import org.junit.Test;
import static org.junit.Assert.*;


public class ProjectCRUDTest extends BasePage {

    @Override
    public void setUp(){
        super.setUp();
    }

    @Test
    public void A_test1createProjectInTheDashboard() {
        loginPage.enterUserEmail(credentials.getUserEmail());
        loginPage.enterUserPassword(credentials.qetUserPassword());
        loginPage.clickLoginButton();
        dashboardPage.newProjectButtonAction();
        dashboardPage.projectName("Project Title Example");
        dashboardPage.createNewProjectButton();
        assertEquals("Should be 'Project Title Example'",
                "Project Title Example",
                webDriverSetup.waitForElement(projectPage.projectTitle()).getText());
    }

    @Test
    public void B_test2renameProjectInTheDashboard() throws InterruptedException {
        loginPage.enterUserEmail(credentials.getUserEmail());
        loginPage.enterUserPassword(credentials.qetUserPassword());
        loginPage.clickLoginButton();
        dashboardPage.actionOnTheProjectRename("New Title For The Project");
        assertEquals("Should be 'New Title For The Project'",
                "New Title For The Project",
                webDriverSetup.waitForElement(dashboardPage.projectName()).getAttribute("title"));

    }

    @Test
    public void C_test3deleteProjectFromTheDashboard() throws InterruptedException {
        loginPage.enterUserEmail(credentials.getUserEmail());
        loginPage.enterUserPassword(credentials.qetUserPassword());
        loginPage.clickLoginButton();
        dashboardPage.actionOnTheProjectDelete();
        assertEquals("Should be 'Nothing here just yet'",
                "Nothing here just yet",
                webDriverSetup.waitForElement(dashboardPage.emptyUngroupedContainer()).getText());
    }

    @Override
    public void tearDown(){
        super.tearDown();
    }
}
