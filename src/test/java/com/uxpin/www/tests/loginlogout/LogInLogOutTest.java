package com.uxpin.www.tests.loginlogout;

import com.uxpin.www.pages.BasePage;
import org.junit.Test;
import static org.junit.Assert.*;

public class LogInLogOutTest extends BasePage {

    @Override
    public void setUp(){

        super.setUp();
    }

    @Test
    public void A_test1LoginLogoutUXPinPositiveScenario(){
        loginPage.enterUserEmail(credentials.getUserEmail());
        loginPage.enterUserPassword(credentials.qetUserPassword());
        loginPage.clickLoginButton();
        assertEquals("Should be 'New Project'",
                "New project",
                webDriverSetup.waitForElement(dashboardPage.newProjectButton()).getText());
        dashboardPage.userAvatar();
        dashboardPage.logOutUser();
        assertEquals("Should be 'Back to UXPin homepage'",
                "Back to UXPin homepage",
                webDriverSetup.waitForElement(loginPage.uxPinLogo()).getAttribute("title"));
    }

    @Test
    public void B_test2LoginUxPinWrongEmailWrongPassword(){
        loginPage.enterUserEmail("asw@df.pl");
        loginPage.enterUserPassword("232weds");
        loginPage.clickLoginButton();
        assertEquals("Should be 'Email or password you entered is incorrect.'",
                "Email or password you entered is incorrect.",
                webDriverSetup.waitForElement(loginPage.wrongEmailPasswordError()).getText());
    }

    @Override
    public void tearDown(){

        super.tearDown();
    }
}
