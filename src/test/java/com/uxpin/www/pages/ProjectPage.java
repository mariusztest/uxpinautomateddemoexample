package com.uxpin.www.pages;

import com.uxpin.www.utilities.WebDriverSetup;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProjectPage {

    WebDriver driver;
    WebDriverSetup webDriverSetup;

    @FindBy(xpath="//div[@class='project-name-select']/h2")
    WebElement projectTitle;

    public ProjectPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebElement projectTitle(){
        return projectTitle;
    }
}
