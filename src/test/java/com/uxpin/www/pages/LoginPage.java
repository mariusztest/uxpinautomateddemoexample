package com.uxpin.www.pages;

import com.uxpin.www.utilities.WebDriverSetup;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

    WebDriver driver;
    WebDriverSetup webDriverSetup;

    @FindBy(xpath="//a[@title='Back to UXPin homepage']")
    WebElement uxPinLogo;

    @FindBy(id="login-login")
    WebElement userEmail;

    @FindBy(id="login-password")
    WebElement userPassword;

    @FindBy(id="loginform_button1")
    WebElement loginButton;

    @FindBy(className = "global-error")
    WebElement errorMessage;


    public LoginPage(WebDriver driver){
        this.driver = driver;
        this.webDriverSetup = new WebDriverSetup(driver);
        PageFactory.initElements(driver, this);
    }

    public WebElement uxPinLogo(){

        return uxPinLogo;
    }

    public void enterUserEmail(String email){

        userEmail.sendKeys(email);
    }

    public void enterUserPassword(String password){

        userPassword.sendKeys(password);
    }

    public void clickLoginButton(){

        loginButton.click();
    }

    public WebElement wrongEmailPasswordError(){

        return errorMessage;
    }
}
