package com.uxpin.www.pages;

import com.uxpin.www.utilities.WebDriverSetup;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DashboardPage {

    WebDriver driver;
    WebDriverSetup webDriverSetup;

    @FindBy(xpath="//a[@class='btn-ghost blue with-plus']")
    WebElement newProjectButton;

    @FindBy(xpath="//figure[@class='user-avatar']")
    WebElement userAvatar;

    @FindBy(xpath="//ul/li/a[contains(text(), 'Log out')]")
    WebElement logOutButton;

    @FindBy(id="project-name")
    WebElement projectNameInput;

    @FindBy(xpath="//button[contains(text(), 'Create New Project')]")
    WebElement createNewProjectButton;

    @FindBy(xpath="//a[@class='header']/h3")
    WebElement projectSection;

    @FindBy(xpath="//a[@class='icon-general-dots only-icon-font']")
    WebElement projectMoreButton;

    @FindBy(xpath="(html/body/ul/li/a)[1]")
    WebElement selectRenameButton;

    @FindBy(xpath="(html/body/ul/li/a)[4]")
    WebElement selectDeleteButton;

    @FindBy(id="project-name")
    WebElement newProjectNameInput;

    @FindBy(xpath="//button[@class='btn-solid blue btn-wide']")
    WebElement saveProjectNameButton;

    @FindBy(xpath = "//label[@class='original-name']")
    WebElement projectName;

    @FindBy(xpath="//button[contains(text(), 'Delete project')]")
    WebElement deleteProjectButton;

    @FindBy(xpath="//a[@class='execute icon-general-close only-icon-font']")
    WebElement executeDelete;

    @FindBy(xpath="(//h3[@class='name'])[1]")
    WebElement emptyUngroupedProjectContainerMessage;


    public DashboardPage(WebDriver driver){
        this.driver = driver;
        this.webDriverSetup = new WebDriverSetup(driver);
        PageFactory.initElements(driver, this);
    }

    public WebElement newProjectButton(){

        return newProjectButton;
    }

    public void newProjectButtonAction(){

        WebElement projectButton = webDriverSetup.waitForElement(newProjectButton);
        projectButton.click();

    }

    public void userAvatar(){

        userAvatar.click();
    }

    public void logOutUser(){

        logOutButton.click();
    }

    public void projectName(String projectName){
        projectNameInput.sendKeys(projectName);
    }

    public void createNewProjectButton(){
        createNewProjectButton.click();
    }

    public WebElement projectName(){
        return projectName;
    }

    public WebElement emptyUngroupedContainer(){
        return emptyUngroupedProjectContainerMessage;
    }

    public void actionOnTheProjectRename(String newProjectName) throws InterruptedException {
        Actions action = new Actions(driver);
        WebElement projectSectionWait = webDriverSetup.waitForElement(projectSection);
        action.moveToElement(projectSectionWait)
                .moveToElement(projectMoreButton)
                .build()
                .perform();
                Thread.sleep(500);
        action.moveToElement(selectRenameButton)
                .click()
                .build()
                .perform();
        newProjectNameInput.clear();
        newProjectNameInput.sendKeys(newProjectName);
        saveProjectNameButton.click();
        Thread.sleep(1000);

    }

    public void actionOnTheProjectDelete() throws InterruptedException {
        Actions action = new Actions(driver);
        WebElement projectSectionWait = webDriverSetup.waitForElement(projectSection);
        action.moveToElement(projectSectionWait)
                .moveToElement(projectMoreButton)
                .build()
                .perform();
        Thread.sleep(500);
        action.moveToElement(selectDeleteButton)
                .click()
                .build()
                .perform();
        deleteProjectButton.click();
        webDriverSetup.waitForElement(executeDelete).click();
    }

}
