package com.uxpin.www.pages;

import com.uxpin.www.utilities.BrowserSetup;
import com.uxpin.www.utilities.Credentials;
import com.uxpin.www.utilities.WebDriverSetup;
import org.openqa.selenium.WebDriver;
import org.junit.Before;
import org.junit.After;


public class BasePage {

    public WebDriver driver;
    BrowserSetup browserSetup;
    public LoginPage loginPage;
    public DashboardPage dashboardPage;
    public WebDriverSetup webDriverSetup;
    public ProjectPage projectPage;
    public Credentials credentials;

    @Before
    public void setUp(){
         browserSetup = new BrowserSetup();
         driver = browserSetup.getBrowser("Chrome");
         browserSetup.setUrl("https://app.uxpin.com/");
         loginPage = new LoginPage(driver);
         dashboardPage = new DashboardPage(driver);
         webDriverSetup = new WebDriverSetup(driver);
         projectPage = new ProjectPage(driver);
         credentials = new Credentials();
         driver.get(browserSetup.getUrl());
    }

    @After
    public void tearDown(){

        driver.quit();
    }
}
