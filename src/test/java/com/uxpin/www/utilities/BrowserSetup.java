package com.uxpin.www.utilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;


public class BrowserSetup {

    private WebDriver driver;
    private String url;

    public WebDriver getBrowser(String browserName){
        if(browserName.equals("Chrome")){
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--start-maximized");
            this.driver = new ChromeDriver(options);

        }else if (browserName.equals("Firefox")){
            this.driver = new FirefoxDriver();
            driver.manage().window().maximize();
        }else {
            System.out.println("No driver detected!");
        }
        return this.driver;
    }

    public void setUrl(String url){

        this.url = url;
    }

    public String getUrl(){

        return url;
    }
}
