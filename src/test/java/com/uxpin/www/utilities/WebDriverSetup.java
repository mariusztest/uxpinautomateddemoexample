package com.uxpin.www.utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebDriverSetup {

    WebDriver driver;
    WebDriverWait wait;

    public WebDriverSetup(WebDriver driver){
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 20);
    }

    public WebElement waitForElement(WebElement element){
        this.wait.until(ExpectedConditions.visibilityOf(element));
        return element;
    }

}
